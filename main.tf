resource "azurerm_resource_group" "myrg" {
  location = var.resource_group_location
  name     = "kejsi-rg"
}

# Create Virtual Network
resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  address_space       = var.vnet_address_space
  location            = var.resource_group_location
  resource_group_name = azurerm_resource_group.myrg.name
}

#  Create Web Subnet
resource "azurerm_subnet" "websubnet" {
  name                 = var.web_subnet_name
  resource_group_name  = azurerm_resource_group.myrg.name
  virtual_network_name = var.vnet_name
  address_prefixes     = var.web_subnet_address  
}

# Create Network Security Group 
resource "azurerm_network_security_group" "web_subnet_nsg" {
  name                = "websubnet-nsg"
  location            = var.resource_group_location
  resource_group_name = azurerm_resource_group.myrg.name
}

#  Associate NSG and Subnet
resource "azurerm_subnet_network_security_group_association" "web_subnet_nsg_associate" {
  depends_on = [ azurerm_network_security_rule.web_nsg_rule_inbound] 
  subnet_id                 = azurerm_subnet.websubnet.id
  network_security_group_id = azurerm_network_security_group.web_subnet_nsg.id
}

#  Create NSG Rules
## Locals Block for Security Rules
locals {
  web_inbound_ports_map = {
    "100" : "80", 
    "110" : "443",
    "120" : "22"
  } 
}
## NSG Inbound Rule for Web Subnets
resource "azurerm_network_security_rule" "web_nsg_rule_inbound" {
  for_each = local.web_inbound_ports_map
  name                        = "Rule-Port-${each.value}"
  priority                    = each.key
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = each.value 
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.myrg.name
  network_security_group_name = azurerm_network_security_group.web_subnet_nsg.name
}


# Create Public IP Address
resource "azurerm_public_ip" "vm_publicip" {
  name                = "vm-kejsi"
  resource_group_name = azurerm_resource_group.myrg.name
  location            = var.resource_group_location
  allocation_method   = "Static"
  sku = "Standard"
}

# Create Network Interface
resource "azurerm_network_interface" "vm_nic" {
  name                = "kejsi-nic"
  location            = var.resource_group_location
  resource_group_name = azurerm_resource_group.myrg.name

  ip_configuration {
    name                          = "vm-ip-1"
    subnet_id                     = azurerm_subnet.websubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.vm_publicip.id 
  }
}

# Azure Linux Virtual Machine
resource "azurerm_linux_virtual_machine" "linuxvm" {
  name = "kejsi-vm"
  resource_group_name = azurerm_resource_group.myrg.name
  location = var.resource_group_location
  size = "Standard_DS1_v2"
  disable_password_authentication = "false"
  admin_username = "kejsiauser"
  admin_password = "Kejsi.123"
  network_interface_ids = [ azurerm_network_interface.vm_nic.id ]
  
  os_disk {
    caching = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  

    
}
